QT += core gui sql

TARGET = Avidya
TEMPLATE = app
INCLUDEPATH += src

HEADERS += \
    src/controller.h \
    src/db/database.h \
    src/model/app.h \
    src/module/hotkey.h \
    src/module/system_tray_icon.h \
    src/module/system_tray_menu.h \
    src/util/global.h \
    src/view/app_list_widget.h \
    src/view/app_name_widget.h \
    src/view/app_painter.h \
    src/view/new_app_window.h \
    src/view/search_window.h \
    src/util/command_parser.h \
    src/service/app_service.h \
    src/service/app_service_thread.h \
    src/view/app_command_widget.h

SOURCES += \
    src/model/app.cc \
    src/view/app_list_widget.cc \
    src/view/app_name_widget.cc \
    src/view/app_painter.cc \
    src/controller.cc \
    src/db/database.cc \
    src/util/global.cc \
    src/module/hotkey.cc \
    src/main.cc \
    src/module/system_tray_icon.cc \
    src/module/system_tray_menu.cc \
    src/view/new_app_window.cc \
    src/view/search_window.cc \
    src/util/command_parser.cc \
    src/service/app_service.cc \
    src/service/app_service_thread.cc \
    src/view/app_command_widget.cc


unix:SOURCES += src/view/search_window_x11.cc
win32:SOURCES += src/view/search_window_win.cc

FORMS += \
    ui/new_app_window.ui \
    ui/search_window.ui

RESOURCES += \
    res/Avidya.qrc

include(lib/qxtglobalshortcut/qxtglobalshortcut.pri)

#DEFINES += QT_NO_CAST_FROM_ASCII












