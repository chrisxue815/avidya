#include "controller.h"

using namespace avidya;

int main(int argc, char **argv)
{
    Controller app(argc, argv);

    return app.exec();
}
