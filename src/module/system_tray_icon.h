#ifndef SYSTEM_TRAY_ICON_H
#define SYSTEM_TRAY_ICON_H

#include <QObject>
#include <QSystemTrayIcon>

class QCoreApplication;

namespace avidya
{
class SearchWindow;
class SystemTrayMenu;

class SystemTrayIcon : public QObject
{
    Q_OBJECT

public:
    SystemTrayIcon(SearchWindow *searchWindow, QCoreApplication *parent = 0);
    ~SystemTrayIcon();

public slots:
    void trayIconActivated(QSystemTrayIcon::ActivationReason reason);

private:
    QSystemTrayIcon *_trayIcon;
    SearchWindow *_searchWindow;
    SystemTrayMenu *_menu;
};
}

#endif // SYSTEM_TRAY_ICON_H
