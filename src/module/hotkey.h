#ifndef HOTKEY_H
#define HOTKEY_H

#include <QObject>

class QxtGlobalShortcut;

namespace avidya
{
class SearchWindow;

class Hotkey : public QObject
{
    Q_OBJECT

public:
    Hotkey(SearchWindow *searchWindow, QObject *parent = 0);
    ~Hotkey();

public slots:
    void hotkeyActivated();

private:
    SearchWindow *_searchWindow;
    QxtGlobalShortcut *_hotkey;
};
}

#endif // HOTKEY_H
