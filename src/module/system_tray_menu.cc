#include "module/system_tray_menu.h"

#include <QCoreApplication>

using namespace avidya;

SystemTrayMenu::SystemTrayMenu(QCoreApplication *application, QWidget *parent) :
    QMenu(parent)
{
    addAction("Show");
    addAction("Options");
    addSeparator();
    QAction * exit = addAction("Exit");
    connect(exit, SIGNAL(triggered()), application, SLOT(quit()));
}
