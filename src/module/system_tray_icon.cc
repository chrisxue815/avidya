#include "module/system_tray_icon.h"

#include <QCoreApplication>

#include "module/system_tray_menu.h"
#include "view/search_window.h"

using namespace avidya;

SystemTrayIcon::SystemTrayIcon(SearchWindow *searchWindow, QCoreApplication *parent) :
    QObject(parent)
{
    _searchWindow = searchWindow;

    _menu = new SystemTrayMenu(parent, searchWindow);

    _trayIcon = new QSystemTrayIcon(QIcon(":/Avidya.ico"), parent);
    _trayIcon->setContextMenu(_menu);

    connect(_trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
        this, SLOT(trayIconActivated(QSystemTrayIcon::ActivationReason)));

    _trayIcon->show();
}

SystemTrayIcon::~SystemTrayIcon()
{
    delete _trayIcon;
}

void SystemTrayIcon::trayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::Context)
        _menu->show();
    else
        _searchWindow->bringToFront();
}
