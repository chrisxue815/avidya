#ifndef SYSTEM_TRAY_MENU_H
#define SYSTEM_TRAY_MENU_H

#include <QMenu>

class QCoreApplication;

namespace avidya
{
class SystemTrayMenu : public QMenu
{
    Q_OBJECT

public:
    explicit SystemTrayMenu(QCoreApplication *application, QWidget *parent = 0);
};
}

#endif // SYSTEM_TRAY_MENU_H
