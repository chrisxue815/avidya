#include "module/hotkey.h"

#include "qxtglobalshortcut.h"

#include "view/search_window.h"

using namespace avidya;

Hotkey::Hotkey(SearchWindow *searchWindow, QObject *parent) :
    QObject(parent)
{
    _searchWindow = searchWindow;

    _hotkey = new QxtGlobalShortcut(QKeySequence("Alt+Q"));

    connect(_hotkey, SIGNAL(activated()),
        this, SLOT(hotkeyActivated()));
}

Hotkey::~Hotkey()
{
    delete _hotkey;
}

void Hotkey::hotkeyActivated()
{
    if (_searchWindow->isActiveWindow()) {
        _searchWindow->hide();
    }
    else {
        _searchWindow->bringToFront();
    }
}
