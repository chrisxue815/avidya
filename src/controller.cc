#include "controller.h"

#include <QMetaType>

#include "module/hotkey.h"
#include "module/system_tray_icon.h"
#include "service/app_service.h"
#include "service/app_service_thread.h"
#include "view/search_window.h"
#include "view/new_app_window.h"

using namespace avidya;

Controller::Controller(int &argc, char **argv) :
    QApplication(argc, argv)
{
    setQuitOnLastWindowClosed(false);

    _appService = new AppService();
    _appServiceThread = new AppServiceThread(_appService);
    _searchWindow = new SearchWindow();
    _newAppWindow = new NewAppWindow();

    qRegisterMetaType<AppList*>("AppList*");

    connect(_searchWindow, SIGNAL(appNameChanged(const QString &)),
        _appServiceThread, SLOT(appNameChanged(const QString &)));

    connect(_appServiceThread, SIGNAL(appFound(AppList*)),
        _searchWindow, SLOT(appFound(AppList*)));

    connect(_searchWindow, SIGNAL(newApp()),
        _newAppWindow, SLOT(show()));

    connect(_newAppWindow, SIGNAL(newApp(App*)),
        _appService, SLOT(insertApp(App*)));

    _appServiceThread->start();
    _searchWindow->show();

    _trayIcon = new SystemTrayIcon(_searchWindow, this);
    _hotkey = new Hotkey(_searchWindow, this);
}

Controller::~Controller()
{
    delete _hotkey;
    delete _trayIcon;
    delete _newAppWindow;
    delete _searchWindow;
    delete _appServiceThread;
    delete _appService;
}
