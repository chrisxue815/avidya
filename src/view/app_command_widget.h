#ifndef APP_COMMAND_WIDGET_H
#define APP_COMMAND_WIDGET_H

#include <QPlainTextEdit>

namespace avidya
{
class AppCommandWidget : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit AppCommandWidget(QWidget *parent = 0);

signals:
    void newApp();

private:
    void keyPressEvent(QKeyEvent *e);
};
}

#endif // APP_COMMAND_WIDGET_H
