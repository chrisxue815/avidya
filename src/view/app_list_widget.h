#ifndef APP_LIST_WIDGET_H
#define APP_LIST_WIDGET_H

#include <QListWidget>
#include <QList>

namespace avidya
{
class App;
class AppPainter;
typedef QList<App*> AppList;

class AppListWidget : public QListWidget
{
    Q_OBJECT

public:
    explicit AppListWidget(QWidget *parent = 0);
    ~AppListWidget();

signals:
    void keyPressed(QKeyEvent *event);

public slots:
    void appFound(AppList *appList);
    void moveUp();
    void moveDown();

private:
    void keyPressEvent(QKeyEvent *event);

    AppPainter *_appPainter;
};
}

#endif // APP_LIST_WIDGET_H
