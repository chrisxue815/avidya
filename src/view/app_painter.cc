#include "view/app_painter.h"

#include <QPainter>

#include "util/global.h"

using namespace avidya;

//TODO: change the hard-code numbers
AppPainter::AppPainter(const QFont &font, const QPalette &palette, QObject *parent) :
    QStyledItemDelegate(parent)
{
    _font = new QFont(font);
    _font->setPointSize(8);
    _nameColor = new QColor(palette.color(QPalette::WindowText));
    _cmdColor = new QColor(Qt::gray);
    _selectedNameColor = new QColor(palette.color(QPalette::HighlightedText));
    _selectedCommandColor = new QColor(palette.color(QPalette::HighlightedText));;
}

AppPainter::~AppPainter()
{
    delete _selectedCommandColor;
    delete _selectedNameColor;
    delete _cmdColor;
    delete _nameColor;
    delete _font;
}

void AppPainter::paint(QPainter *painter, const QStyleOptionViewItem &option,
    const QModelIndex &index) const
{
    painter->save();

    QColor *nameColor;
    QColor *cmdColor;

    // selected
    if (option.state & QStyle::State_Selected) {
        painter->fillRect(option.rect, option.palette.highlight());
        nameColor = _selectedNameColor;
        cmdColor = _selectedCommandColor;
    }
    else {
        nameColor = _nameColor;
        cmdColor = _cmdColor;
    }

    // calculate positions
    QRect iconRect = option.rect;
    iconRect.setTop(iconRect.top() + 4);
    iconRect.setWidth(Global::ICON_WIDTH);
    iconRect.setHeight(Global::ICON_HEIGHT);

    int fontHeight = painter->fontMetrics().height();
    int nameLeft = iconRect.left() + Global::ICON_WIDTH + 10;
    int nameBottom = iconRect.top() + fontHeight;

    QRect nameRect = option.rect;
    nameRect.setLeft(nameLeft);
    nameRect.setBottom(nameBottom);

    QRect cmdRect = option.rect;
    cmdRect.setLeft(nameLeft);
    cmdRect.setTop(nameBottom);

    // draw
    QImage icon = index.data(Global::APP_ICON).value<QImage>();
    painter->drawImage(iconRect, icon);

    QString name = index.data(Global::APP_NAME).toString();
    painter->setPen(*nameColor);
    painter->drawText(nameRect, Qt::AlignBottom, name);

    QString cmd = index.data(Global::APP_COMMAND).toString();
    painter->setPen(*cmdColor);
    painter->setFont(*_font);
    painter->drawText(cmdRect, Qt::AlignTop, cmd);

    painter->restore();
}

QSize AppPainter::sizeHint(const QStyleOptionViewItem &,
    const QModelIndex &) const
{
    return QSize(0, Global::APP_ITEM_HEIGHT);
}
