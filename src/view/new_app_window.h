#ifndef NEW_APP_WINDOW_H
#define NEW_APP_WINDOW_H

#include <QWidget>

namespace Ui {
    class NewAppWindow;
}

namespace avidya
{
class App;

class NewAppWindow : public QWidget
{
    Q_OBJECT

public:
    explicit NewAppWindow(QWidget *parent = 0);
    ~NewAppWindow();

signals:
    void newApp(App *app);

private slots:
    void okClicked();

private:
    void showEvent(QShowEvent *);


    Ui::NewAppWindow *_ui;
};
}

#endif // NEW_APP_WINDOW_H
