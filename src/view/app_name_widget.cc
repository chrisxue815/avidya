#include "view/app_name_widget.h"

#include <QKeyEvent>

using namespace avidya;

AppNameWidget::AppNameWidget(QWidget *parent) :
    QLineEdit(parent)
{
    setFocus();
}

void AppNameWidget::keyPressed(QKeyEvent *event)
{
    setFocus();
    keyPressEvent(event);
}

void AppNameWidget::keyPressEvent(QKeyEvent *event)
{
    bool accepted = false;

    if (event->modifiers() == Qt::ControlModifier)
    {
        switch (event->key())
        {
        case Qt::Key_D:
            emit openFolder();
            accepted = true;
            break;

        case Qt::Key_N:
            emit newApp();
            accepted = true;
            break;
        }
    }
    else
    {
        switch (event->key())
        {
        case Qt::Key_Up:
            emit moveUp();
            accepted = true;
            break;

        case Qt::Key_Down:
            emit moveDown();
            accepted = true;
            break;

        case Qt::Key_Return:
        case Qt::Key_Enter:
            emit launchApp();
            accepted = true;
            break;

        case Qt::Key_Escape:
            emit hideWindow();
            accepted = true;
            break;
        }
    }

    if (!accepted)
        QLineEdit::keyPressEvent(event);
}
