#ifndef APP_NAME_WIDGET_H
#define APP_NAME_WIDGET_H

#include <QLineEdit>

namespace avidya
{
class AppNameWidget : public QLineEdit
{
    Q_OBJECT

public:
    explicit AppNameWidget(QWidget *parent = 0);

signals:
    void moveUp();
    void moveDown();
    void launchApp();
    void openFolder();
    void newApp();
    void hideWindow();

public slots:
    void keyPressed(QKeyEvent *event);

private:
    void keyPressEvent(QKeyEvent *event);
};
}

#endif // APP_NAME_WIDGET_H
