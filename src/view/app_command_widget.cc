#include "app_command_widget.h"

using namespace avidya;

AppCommandWidget::AppCommandWidget(QWidget *parent) :
    QPlainTextEdit(parent)
{
}

void AppCommandWidget::keyPressEvent(QKeyEvent *e)
{
    switch (e->key())
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        emit newApp();
        break;

    default:
        QPlainTextEdit::keyPressEvent(e);
    }
}
