#include "view/new_app_window.h"

#include "ui_new_app_window.h"
#include "model/app.h"

using namespace avidya;

NewAppWindow::NewAppWindow(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::NewAppWindow)
{
    _ui->setupUi(this);
    setFixedSize(width(), height());

    connect(_ui->okButton, SIGNAL(clicked()),
        this, SLOT(okClicked()));

    connect(_ui->cmdEdit, SIGNAL(newApp()),
        _ui->okButton, SLOT(click()));
}

NewAppWindow::~NewAppWindow()
{
    delete _ui;
}

void NewAppWindow::okClicked()
{
    QString name = _ui->nameEdit->text();
    QString cmd = _ui->cmdEdit->toPlainText();

    App *app = new App(name, cmd);

    emit newApp(app);

    hide();
}

void NewAppWindow::showEvent(QShowEvent *)
{
    _ui->nameEdit->setText("");
    _ui->cmdEdit->setPlainText("");
    _ui->nameEdit->setFocus();
}
