#include "view/search_window.h"

using namespace avidya;

void SearchWindow::bringToFront()
{
    show();
    raise();
    activateWindow();
}
