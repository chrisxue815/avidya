#ifndef SEARCH_WINDOW_H
#define SEARCH_WINDOW_H

#include <QMoveEvent>
#include <QWidget>
#include <QList>

namespace Ui {
    class SearchWindow;
}

namespace avidya
{
class App;
class AppListWidget;
typedef QList<App*> AppList;

class SearchWindow : public QWidget
{
    Q_OBJECT

public:
    explicit SearchWindow(QWidget *parent = 0);
    ~SearchWindow();

    void bringToFront();

signals:
    void appNameChanged(const QString &appName);
    void newApp();

public slots:
    void appFound(AppList *appList);

private slots:
    void textChanged(const QString &text);
    void launchApp();
    void openFolder();
    void openNewAppWindow();

private:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void showEvent(QShowEvent *event);

    Ui::SearchWindow *_ui;
    int _oldX;
    int _oldY;
};
}

#endif // SEARCH_WINDOW_H
