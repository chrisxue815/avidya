#include "view/app_list_widget.h"

#include "view/app_painter.h"
#include "model/app.h"
#include "util/global.h"

using namespace avidya;

AppListWidget::AppListWidget(QWidget *parent) :
    QListWidget(parent)
{
    setWindowFlags(Qt::FramelessWindowHint);
    _appPainter = new AppPainter(font(), palette(), this);
    setItemDelegate(_appPainter);
}

AppListWidget::~AppListWidget()
{
    delete _appPainter;
}

void AppListWidget::appFound(AppList *appList)
{
    clear();

    foreach (const App *app, *appList)
    {
        QListWidgetItem *item = new QListWidgetItem(this);
        item->setData(Global::APP_ICON, app->getIcon());
        item->setData(Global::APP_NAME, app->getName());
        item->setData(Global::APP_COMMAND, app->getCommand());
    }

    setCurrentRow(0);
}

void AppListWidget::moveUp()
{
    int cur = currentRow();
    if (cur > 0)
        setCurrentRow(cur - 1);
}

void AppListWidget::moveDown()
{
    int cur = currentRow();
    if (cur < count() - 1)
        setCurrentRow(cur + 1);
}

void AppListWidget::keyPressEvent(QKeyEvent *event)
{
    emit keyPressed(event);
}
