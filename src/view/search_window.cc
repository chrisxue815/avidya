#include "view/search_window.h"

#include <QProcess>
#include <QDesktopServices>
#include <QUrl>

#include "ui_search_window.h"
#include "util/command_parser.h"
#include "util/global.h"
#include "view/app_list_widget.h"

using namespace avidya;

SearchWindow::SearchWindow(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::SearchWindow)
{
    setWindowFlags(Qt::FramelessWindowHint);

    _ui->setupUi(this);

    connect(_ui->appNameWidget, SIGNAL(textChanged(QString)),
        this, SLOT(textChanged(QString)));

    connect(_ui->appNameWidget, SIGNAL(launchApp()),
        this, SLOT(launchApp()));

    connect(_ui->appNameWidget, SIGNAL(openFolder()),
        this, SLOT(openFolder()));

    connect(_ui->appNameWidget, SIGNAL(newApp()),
        this, SLOT(openNewAppWindow()));

    connect(_ui->appListWidget, SIGNAL(keyPressed(QKeyEvent*)),
        _ui->appNameWidget, SLOT(keyPressed(QKeyEvent*)));

    connect(_ui->appNameWidget, SIGNAL(moveUp()),
        _ui->appListWidget, SLOT(moveUp()));

    connect(_ui->appNameWidget, SIGNAL(moveDown()),
        _ui->appListWidget, SLOT(moveDown()));

    connect(_ui->appNameWidget, SIGNAL(hideWindow()),
        this, SLOT(hide()));
}

SearchWindow::~SearchWindow()
{
    delete _ui;
}

void SearchWindow::appFound(AppList *appList)
{
    static int basic1 = height() - _ui->appListWidget->height();
    static int basic2 = basic1 - _ui->appListWidget->y()
            + _ui->appNameWidget->y() + _ui->appNameWidget->height();

    int newHeight;

    if (appList->count() == 0) {
        _ui->appListWidget->hide();
        newHeight = basic2;
    }
    else {
        _ui->appListWidget->appFound(appList);
        _ui->appListWidget->show();

        int count = appList->count();
        if (count > Global::MAX_APP_NUM)
            count = Global::MAX_APP_NUM;

        newHeight = basic1 + count * Global::APP_ITEM_HEIGHT + 10;
    }

    setFixedSize(width(), newHeight);
}

void SearchWindow::textChanged(const QString &text)
{
    emit appNameChanged(text);
}

void SearchWindow::launchApp()
{
    //TODO: remember to add "" before inserting an app
    QString cmd = _ui->appListWidget->currentItem()->data(Global::APP_COMMAND).toString();
    QProcess::startDetached(cmd);
}

void SearchWindow::openFolder()
{
    QString cmd = _ui->appListWidget->currentItem()->data(Global::APP_COMMAND).toString();
    QString folder = CommandParser(cmd).getFolderInUrl();
    QDesktopServices::openUrl(QUrl(folder));
    qDebug("%s", folder.toStdString().c_str());
}

void SearchWindow::openNewAppWindow()
{
    emit newApp();
}

void SearchWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        _oldX = event->globalX();
        _oldY = event->globalY();
    }
}

void SearchWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton)
    {
        int newX = event->globalX();
        int newY = event->globalY();

        int posX = newX - _oldX + x();
        int posY = newY - _oldY + y();

        move(posX, posY);

        _oldX = newX;
        _oldY = newY;
    }
}

void SearchWindow::showEvent(QShowEvent *)
{
    static bool firstShow = true;

    if (firstShow)
    {
        firstShow = false;
        emit appNameChanged(_ui->appNameWidget->text());
    }
    else
    {
        _ui->appNameWidget->selectAll();
    }
}
