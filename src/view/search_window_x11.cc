#include "view/search_window.h"

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <QX11Info>

using namespace avidya;

void wmMessage(Window win, long type, long l0, long l1, long l2, long l3, long l4)
{
    XClientMessageEvent xev;

    xev.type = ClientMessage;
    xev.window = win;
    xev.message_type = type;
    xev.format = 32;
    xev.data.l[0] = l0;
    xev.data.l[1] = l1;
    xev.data.l[2] = l2;
    xev.data.l[3] = l3;
    xev.data.l[4] = l4;

    XSendEvent(QX11Info::display(), QX11Info::appRootWindow(), False,
        (SubstructureNotifyMask | SubstructureRedirectMask),
        (XEvent *)&xev);
}

void SearchWindow::bringToFront()
{
    show();

    // just in case we don't have WM running
    raise();
    activateWindow();

    // activate window
    static Atom NET_ACTIVE_WINDOW = XInternAtom(QX11Info::display(),
        "_NET_ACTIVE_WINDOW", False);
    wmMessage(winId(), NET_ACTIVE_WINDOW, 2, CurrentTime, 0, 0, 0);
}
