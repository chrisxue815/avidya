#ifndef LIST_ITEM_PAINTER_H
#define LIST_ITEM_PAINTER_H

#include <QFont>
#include <QStyledItemDelegate>

namespace avidya
{
class AppPainter : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit AppPainter(const QFont &font, const QPalette &palette, QObject *parent = 0);
    ~AppPainter();

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
        const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    QFont *_font;
    QColor *_nameColor;
    QColor *_cmdColor;
    QColor *_selectedNameColor;
    QColor *_selectedCommandColor;
};
}

#endif // LIST_ITEM_PAINTER_H
