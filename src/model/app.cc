#include "model/app.h"

#include <QFileInfo>
#include <QFileIconProvider>

#include "util/command_parser.h"
#include "util/global.h"

using namespace avidya;

App::App(const QString &name, const QString &cmd) :
    _id(-1), _name(name)
{
    setCommand(cmd);
    setIcon(_cmd);
}

App::App(long long id, const QString &name, const QString &cmd, const QImage &icon) :
    _id(id), _name(name), _cmd(cmd)
{
    setIcon(icon);
}

void App::setCommand(const QString &cmd)
{
    if (cmd.length() > 0 && cmd[0] != '"' && cmd.contains(' '))
    {
        _cmd = QLatin1String("\"");
        _cmd.append(cmd);
        _cmd.append('"');
    }
    else
    {
        _cmd = cmd;
    }
}

void App::setIcon(const QString &cmd)
{
    const QString path = CommandParser(cmd).getPath();

    //TODO: test with parameters
    QFileInfo file(path);
    QFileIconProvider iconProvider;
    const QIcon &icon = iconProvider.icon(file);
    const QPixmap &pixmap = icon.pixmap(Global::ICON_WIDTH, Global::ICON_HEIGHT);
    setIcon(pixmap.toImage());
}

void App::setIcon(const QImage &icon)
{
    if (icon.width() != Global::ICON_WIDTH && icon.height() != Global::ICON_WIDTH)
    {
        _icon = icon.scaled(
            Global::ICON_WIDTH, Global::ICON_WIDTH,
            Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }
    else
    {
        _icon = icon;
    }
}
