#ifndef APP_H
#define APP_H

#include <QImage>
#include <QString>

namespace avidya
{
class App
{
public:
    App() {}
    App(const QString &name, const QString &cmd);
    App(long long id, const QString &name, const QString &cmd, const QImage &icon);

    long long getId() const { return _id; }
    void setId(long long id) { _id = id; }

    const QString &getName() const { return _name; }
    void setName(const QString &name) { _name = name; }

    const QString &getCommand() const { return _cmd; }
    void setCommand(const QString &cmd);

    const QImage &getIcon() const { return _icon; }
    void setIcon(const QImage &_icon);
    void setIcon(const QString &cmd);

private:
    long long _id;
    QString _name;
    QString _cmd;
    QImage _icon;
};
}

#endif // APP_H
