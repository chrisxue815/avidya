#include "service/app_service_thread.h"

#include "service/app_service.h"

using namespace avidya;

AppServiceThread::AppServiceThread(AppService *appService)
{
    _appService = appService;
    _stopped = false;
}

AppServiceThread::~AppServiceThread()
{
    _stopped = true;
    _waitForAppName.wakeAll();
    wait();
}

void AppServiceThread::run()
{
    forever
    {
        _mutex.lock();
        _waitForAppName.wait(&_mutex);
        _mutex.unlock();

        if (_stopped)
            break;

        _appService->findAppByName(_appName, _result);

        emit appFound(&_result);
    }
}

void AppServiceThread::appNameChanged(const QString &appName)
{
    _appName = appName;
    _waitForAppName.wakeAll();
}
