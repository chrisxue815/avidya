#include "service/app_service.h"

#include <QMultiMap>

#include "db/database.h"
#include "model/app.h"

using namespace avidya;

AppService::AppService()
{
    _db = new Database();
    _db->findApp(_appList);
}

AppService::~AppService()
{
    foreach(App *app, _appList)
    {
        delete app;
    }
    delete _db;
}

void AppService::findAppByName(QString &pattern, AppList &result, int maxNum)
{
    QMultiMap<int, App*> map;

    foreach (App *app, _appList)
    {
        const QString &main = app->getName();
        int index = main.indexOf(pattern, 0, Qt::CaseInsensitive);

        if (index != -1) {
            int weight = (index << 16) + main.length();
            map.insert(weight, app);
        }
    }

    if (map.size() <= maxNum) {
        result = map.values();
    }
    else
    {
        result.clear();
        result.reserve(maxNum);
        QMap<int, App*>::iterator it = map.begin();

        for (int i = 0; i < maxNum; ++i) {
            result.append(*it);
            ++it;
        }
    }
}

void AppService::insertApp(App *app)
{
    _db->insertApp(*app);

    _appList.append(app);
}
