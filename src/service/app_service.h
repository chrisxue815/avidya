#ifndef APP_SERVICE_H
#define APP_SERVICE_H

#include <QObject>
#include <QList>

namespace avidya
{
class App;
class Database;

typedef QList<App*> AppList;

class AppService : public QObject
{
    Q_OBJECT

public:
    static const int MAX_APP_NUM = 8;

    AppService();
    ~AppService();

    void findAppByName(QString &pattern, AppList &result, int maxNum = MAX_APP_NUM);

public slots:
    void insertApp(App *app);

private:
    Database *_db;
    AppList _appList;
};
}

#endif // APP_SERVICE_H
