#ifndef APP_SERVICE_THREAD_H
#define APP_SERVICE_THREAD_H

#include <QList>
#include <QMutex>
#include <QThread>
#include <QWaitCondition>

namespace avidya
{
class App;
class AppService;
typedef QList<App*> AppList;

class AppServiceThread : public QThread
{
    Q_OBJECT

public:
    AppServiceThread(AppService *appService);
    ~AppServiceThread();

    void run();

signals:
    void appFound(AppList *appList);

public slots:
    void appNameChanged(const QString &appName);

private:
    AppService *_appService;
    QString _appName;
    AppList _result;
    QMutex _mutex;
    QWaitCondition _waitForAppName;
    bool _stopped;
};
}

#endif // APP_SERVICE_THREAD_H
