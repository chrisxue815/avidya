#ifndef COMMAND_PARSER_H
#define COMMAND_PARSER_H

#include <QString>

namespace avidya
{
class CommandParser
{
public:
    CommandParser(const QString &cmd);

    QString getPath();
    QString getFolder();
    QString getFolderInUrl();

private:
    const QString &_cmd;
};
}

#endif // COMMAND_PARSER_H
