#include "command_parser.h"

#include <QDir>

using namespace avidya;

CommandParser::CommandParser(const QString &cmd) :
    _cmd(cmd)
{
}

QString CommandParser::getPath()
{
    if (_cmd.count() > 0)
    {
        if (_cmd[0] == '"')
        {
            bool found = false;
            int index;

            for (int i = 1; i < _cmd.count(); i++)
            {
                if (_cmd[i] == '"')
                {
                    found = true;
                    index = i;
                    break;
                }
            }

            if (found)
                return _cmd.mid(1, index - 1);
        }
        else
        {
            int index = _cmd.indexOf(' ');
            if (index == -1)
                return _cmd;
            else
                return _cmd.left(index + 1);
        }
    }

    throw QString("Invalid command");
}

QString CommandParser::getFolder()
{
    QString path = getPath();
    int index = path.lastIndexOf(QDir::separator());
    return path.left(index + 1);
}

QString CommandParser::getFolderInUrl()
{
    QString folder = getFolder();

    if (QDir::separator() == '\\')
    {
        folder.replace('\\', '/');
        return QString("file:///").append(folder);
    }
    else
    {
        return folder;
    }
}
