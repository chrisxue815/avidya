#ifndef GLOBAL_H
#define GLOBAL_H

#include <qnamespace.h>

namespace avidya
{
class Global
{
public:
    static const int APP_ITEM_HEIGHT = 40;
    static const int MAX_APP_NUM = 5;

    static const int ICON_WIDTH = 32;
    static const int ICON_HEIGHT = 32;

    static const int APP_ICON = Qt::DecorationRole;
    static const int APP_NAME = Qt::DisplayRole;
    static const int APP_COMMAND = Qt::ToolTipRole;
};
}

#endif // GLOBAL_H
