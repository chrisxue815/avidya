#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QApplication>

namespace avidya
{
class SearchWindow;
class NewAppWindow;
class Hotkey;
class SystemTrayIcon;
class AppService;
class AppServiceThread;

class Controller : public QApplication
{
    Q_OBJECT

public:
    Controller(int &argc, char **argv);
    ~Controller();

private:
    SearchWindow *_searchWindow;
    NewAppWindow *_newAppWindow;
    Hotkey *_hotkey;
    SystemTrayIcon *_trayIcon;
    AppService *_appService;
    AppServiceThread *_appServiceThread;
};
}

#endif // CONTROLLER_H
