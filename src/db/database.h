#ifndef DATABASE_H
#define DATABASE_H

#include <QList>
#include <QSqlDatabase>

namespace avidya
{
class App;
typedef QList<App*> AppList;

class Database
{
public:
    Database();
    ~Database();

    void init();

    void findApp(AppList &appList);
    void insertApp(const App &app);

private:
    static const QLatin1String DATABASE_NAME;

    static const QLatin1String TABLE_VERSION;
    static const QLatin1String TABLE_APP;

    QSqlDatabase _db;
};
}

#endif // DATABASE_H
