#include "db/database.h"

#include <QBuffer>
#include <QDir>
#include <QSqlError>
#include <QSqlQuery>
#include <QStringList>
#include <QVariant>

#include "model/app.h"

using namespace avidya;

const QLatin1String Database::DATABASE_NAME("db.sqlite");

const QLatin1String Database::TABLE_VERSION("Version");
const QLatin1String Database::TABLE_APP("App");

Database::Database()
{
    _db = QSqlDatabase::addDatabase(QLatin1String("QSQLITE"));

    QDir().remove(DATABASE_NAME);

    _db.setDatabaseName(DATABASE_NAME);
    _db.open();

    //TODO: debug
    init();
    /*if (!db.tables().contains(TABLE_VERSION, Qt::CaseInsensitive)) {
        init();
    }*/
}

Database::~Database()
{
}

void Database::init()
{
    static const QLatin1String CREATE_TABLE_VERSION(
        "create table Version(version integer primary key)");

    static const QLatin1String CREATE_TABLE_APP(
        "create table App(id integer primary key, name unique, cmd, icon)");

    static const QLatin1String INSERT_INTO_VERSION(
        "insert into Version values(1)");

    _db.exec(CREATE_TABLE_VERSION);
    _db.exec(CREATE_TABLE_APP);
    _db.exec(INSERT_INTO_VERSION);

    // TODO: delete this
    _db.transaction();
    insertApp(App("Chrome", "C:\\Users\\Chris\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe"));
    insertApp(App("Firefox", "\"Z:\\Program Files\\Mozilla Firefox\\firefox.exe\""));
    insertApp(App("apt", "\"z:\\program files\\apt\\apt.exe\""));
    insertApp(App("ie", "iexplore.exe"));
    insertApp(App("foxit", "\"z:\\program\\Foxit Software\\Foxit PhantomPDF\\Foxit PhantomPDF.exe\" \"z:\\data1\\document\\FDTD_simp._Chinese.pdf\""));
    /*for (int i = 0; i < 10000; i++)
    {
        insertApp(App("Many many many Firefox %1", "/usr/lib/firefox-9.0.1/firefox')"));
    }*/
    _db.commit();
}

void Database::findApp(AppList &appList)
{
    static const QLatin1String SELECT_APP(
        "select * from App");

    QSqlQuery q(_db);
    q.exec(SELECT_APP);

    appList.clear();
    appList.reserve(q.size());

    while (q.next()) {
        long long id = q.value(0).toLongLong();
        QString name = q.value(1).toString();
        QString cmd = q.value(2).toString();
        QImage icon;
        icon.loadFromData(q.value(3).toByteArray());
        appList.append(new App(id, name, cmd, icon));
    }
}

void Database::insertApp(const App &app)
{
    static const QLatin1String INSERT_NEW_APP(
        "insert or replace into App values(NULL, :name, :cmd, :icon)");

    static const QLatin1String INSERT_EXISTED_APP(
        "insert or replace into App values(:id, :name, :cmd, :icon)");

    QSqlQuery q(_db);

    if (app.getId() < 0) {
        q.prepare(INSERT_NEW_APP);
    }
    else {
        q.prepare(INSERT_EXISTED_APP);
        q.bindValue(":id", app.getId());
    }

    q.bindValue(":name", app.getName());
    q.bindValue(":cmd", app.getCommand());

    // save the icon into a byte array
    QByteArray bytes;
    QBuffer buff(&bytes);
    buff.open(QIODevice::WriteOnly);
    app.getIcon().save(&buff, "PNG");

    q.bindValue(":icon", bytes);

    q.exec();
}
